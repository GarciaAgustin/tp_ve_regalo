using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class PlatformerGame_Manager : MonoBehaviour, ISaveable
{
    public static PlatformerGame_Manager Instance;

    [SerializeField] private List<GameObject> Cookies = new List<GameObject>();

    public int CookiesNumber;

    private GameObject Player;
    private Vector3 playerPosition;

    public bool ThePlayerHasWin;
    public bool ThePlayerLoose;

    [SerializeField] private TextMeshProUGUI TXTCookies;

    public GameObject WinAndLosePanel;

    Color WinOrLoosePanelColor;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        Player = GameObject.FindWithTag("Player"); 
        playerPosition = Player.transform.position;

        CookiesNumber = Cookies.Count;
    }

    void Update()
    {
        TXTCookies.text = CookiesNumber.ToString();

        if (CookiesNumber == 0)
        {
            ThePlayerHasWin = true;
            Player.transform.position = playerPosition;
        }

        ShowWinAndLoosePanel();
    }

    public void Restart()
    {
        Player.transform.position = playerPosition;
        ThePlayerLoose = false;
        ThePlayerHasWin = false;
        WinAndLosePanel.gameObject.SetActive(false);
        Player.gameObject.SetActive(true);
        foreach (GameObject cookie in Cookies)
        {
            cookie.gameObject.SetActive(true);
        }
        CookiesNumber = Cookies.Count;
    }

    public object CaptureState()
    {
        return new SaveData
        {
            playerPositionX = Player.transform.position.x,
            playerPositionY = Player.transform.position.y,
            playerPositionZ = Player.transform.position.z
        };
    }

    public void RestoreState(object state)
    {
        var saveData = (SaveData)state;
        Player.transform.position = new Vector3(saveData.playerPositionX, saveData.playerPositionY, saveData.playerPositionZ);
    }

    [Serializable]
    private struct SaveData
    {
        public float playerPositionX;
        public float playerPositionY;
        public float playerPositionZ;
    }


    public void ShowWinAndLoosePanel()
    {
        if (WinAndLosePanel != null && ThePlayerHasWin == true)
        {
            ColorUtility.TryParseHtmlString("#4DA454", out WinOrLoosePanelColor);

            WinAndLosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Win";
            WinAndLosePanel.gameObject.SetActive(true);
        }
        else if (WinAndLosePanel != null && ThePlayerLoose == true)
        {
            ColorUtility.TryParseHtmlString("#9C2424", out WinOrLoosePanelColor);

            WinAndLosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Lose";
            WinAndLosePanel.gameObject.SetActive(true);
        }

    }
}
