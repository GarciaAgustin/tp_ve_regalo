using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using System.Collections;

public class Simple_PlayerController : MonoBehaviour
{
    private float horizontal;
    private float vertical;

    private Rigidbody2D rb;

    public bool isGrounded;
    public Transform groundCheck;
    public int jumps;
    public float playerSpeed;
    public float playerJumpForce;

    public List<Gamepad> pads = new List<Gamepad>();

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
       
    }

    void Start()
    {
      
        var devices = InputSystem.devices;

        foreach (var device in devices)
        {
            if (device is Gamepad gamepad)
            {
                Debug.Log("Gamepad detected: " + gamepad.displayName);
                pads.Add(gamepad);
            }
        }
    }

    private void Update()
    {
        if(UI_Manager.Instance.TheUIisOpen == false)
        {
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            Move();
        }
        else
        {
            rb.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    private void Move()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        if (pads.Count > 0 && pads[0].buttonSouth.wasPressedThisFrame && jumps > 0 || Keyboard.current.spaceKey.wasPressedThisFrame && jumps > 0)
        {
            PerformJump();
        }

        rb.velocity = new Vector3(horizontal * playerSpeed, rb.velocity.y);

        if (horizontal < 0)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            gameObject.GetComponentInChildren<Animator>().SetBool("PlayerWalk", true);
        }
        else if (horizontal > 0)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            gameObject.GetComponentInChildren<Animator>().SetBool("PlayerWalk", true);
        }
        else
        {
            gameObject.GetComponentInChildren<Animator>().SetBool("PlayerWalk", false);
        }
    }

    private void PerformJump()
    {
        rb.velocity = Vector2.up * playerJumpForce;
        jumps--;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            isGrounded = true;
            jumps = 2;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            isGrounded = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Cookie"))
        {
            Audio_Manager.Instance.Play("Score");

            collision.gameObject.SetActive(false);

            PlatformerGame_Manager.Instance.CookiesNumber -= 1;
        }
        else if (collision.CompareTag("Spikes"))
        {
            PlatformerGame_Manager.Instance.ThePlayerLoose = true;
            gameObject.SetActive(false);
            Audio_Manager.Instance.Play("Death");
        }
    }
  
}
