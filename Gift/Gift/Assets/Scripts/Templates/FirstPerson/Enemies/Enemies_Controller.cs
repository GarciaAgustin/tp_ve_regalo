using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies_Controller : MonoBehaviour
{

    GameObject Player;

    [SerializeField]
    GameObject Bullet;

    [SerializeField]
    GameObject ShootPoint;

    [SerializeField]
    float BulletForce;

    public float TimeBetweenShoots;

    public float StartTimeBetweenShoots;

    public bool CanShoot;

    private void Awake()
    {
        CanShoot = true;
        Player = GameObject.FindWithTag("Player");
    }

    void Start()
    {
        
    }

    void Update()
    {
        transform.LookAt(Player.transform);

    }

   public void ShootingBullets()
   {

        if (TimeBetweenShoots <= 0 && CanShoot == true)
        {
           GameObject bullet = Instantiate(Bullet, ShootPoint.transform.position, Quaternion.identity);

           Bullet.GetComponent<Rigidbody>().AddForce(ShootPoint.transform.forward * BulletForce, ForceMode.Impulse);

            Destroy(bullet, 5);
            TimeBetweenShoots = StartTimeBetweenShoots;
        }
        else
        {
            TimeBetweenShoots -= Time.deltaTime;
        }

   }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            CanShoot = false;
            gameObject.SetActive(false);
        }
    }

}
