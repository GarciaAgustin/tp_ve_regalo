using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NPCdialogue_Controller : MonoBehaviour
{

    [SerializeField]
    GameObject DialoguePanel;

    public TextMeshProUGUI DialogueText;

    public string[] Dialogue;

    public int Index;

    public float WordSpeed;

    [SerializeField]
    GameObject ContinueButton;

    void Start()
    {
        
    }


    void Update()
    {
        
    }

    public void StartTalking()
    {
        UI_Manager.Instance.TheUIisOpen = true;

        if(DialoguePanel.activeInHierarchy == true)
        {
            ResetText();
        }
        else
        {
            DialoguePanel.SetActive(true);
            StartCoroutine(Talking());
        }

        if(DialogueText.text == Dialogue[Index])
        {
            ContinueButton.SetActive(true);
        }
    }

    public void ResetText()
    {
        UI_Manager.Instance.TheUIisOpen = false;
        DialogueText.text = "";
        Index = 0;
        DialoguePanel.SetActive(false);
    }

    IEnumerator Talking()
    {
        foreach(char letter in Dialogue[Index])
        {
            DialogueText.text += letter;
            yield return new WaitForSeconds(WordSpeed);
        }
    }

    public void NextLine()
    {

        if (Index < Dialogue.Length - 1)
        {
            Index++;
            DialogueText.text = "";
            StartCoroutine(Talking());
        }
        else
        {
            ResetText();
        }
    }
}
