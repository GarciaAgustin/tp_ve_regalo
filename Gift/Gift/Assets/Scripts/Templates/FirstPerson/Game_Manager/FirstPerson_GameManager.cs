using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.InputSystem;

public class FirstPerson_GameManager : MonoBehaviour
{

    public static FirstPerson_GameManager Instance;

    [SerializeField]
    TextMeshProUGUI Timer;

    [SerializeField]
    float GameTime;

    float InitalGameTime;

    GameObject Player;

    public Vector3 PlayerPosition;

    [SerializeField]
    GameObject Door;

    public List<GameObject> Enemies = new List<GameObject>();

    public GameObject WinAndLoosePanel;

    public bool thePlayerHasWon;
    public bool thePlayerLoose;

    Color WinOrLoosePanelColor;

    bool GameIsInPause;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        Player = GameObject.FindWithTag("Player");

        PlayerPosition = Player.transform.position;

        InitalGameTime = GameTime;

        GameIsInPause = false;
    }

    void Start()
    {
        
    }


    void Update()
    {

        if(thePlayerHasWon == false && thePlayerLoose == false)
        {
            GameTime -= Time.deltaTime;

            Timer.text = GameTime.ToString("F0");
        }

        if(GameTime < 1)
        {
            RestartGame();
        }

        if(Door.GetComponent<Door_Controller>().IsOpen == true)
        {
           foreach(GameObject e in Enemies)
           {
             e.GetComponent<Enemies_Controller>().ShootingBullets();
           }
        }

        ShowWinAndLoosePanel();

        if (Keyboard.current.tabKey.wasPressedThisFrame)
        {
            if(GameIsInPause == false)
            {
                Pause();
            }
            else if (GameIsInPause == true)
            {
                UI_Manager.Instance.CloseOptions();
                QuitPause();
            }
        }

    }

    public void RestartGame()
    {
        UI_Manager.Instance.TheUIisOpen = false;

        thePlayerHasWon = false;

        thePlayerLoose = false;

        WinAndLoosePanel.gameObject.SetActive(false);

        Player.transform.position = PlayerPosition;

        GameTime = InitalGameTime;

        Door.GetComponent<Animator>().SetBool("Open", false);

        Door.GetComponent<Door_Controller>().IsOpen = false;

        foreach (GameObject e in Enemies)
        {
            e.SetActive(true);
            e.GetComponent<Enemies_Controller>().CanShoot = true;
        }
    }

    public void ShowWinAndLoosePanel()
    {
        if (thePlayerHasWon == true)
        {
            UI_Manager.Instance.TheUIisOpen = true;

            Door.GetComponent<Door_Controller>().IsOpen = false;

            ColorUtility.TryParseHtmlString("#4DA454", out WinOrLoosePanelColor);

            WinAndLoosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLoosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Win";
            WinAndLoosePanel.gameObject.SetActive(true);
        }
        else if (thePlayerLoose == true)
        {
            UI_Manager.Instance.TheUIisOpen = true;

            Door.GetComponent<Door_Controller>().IsOpen = false;

            ColorUtility.TryParseHtmlString("#9C2424", out WinOrLoosePanelColor);

            WinAndLoosePanel.GetComponent<Image>().color = WinOrLoosePanelColor;
            WinAndLoosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Lose";
            WinAndLoosePanel.gameObject.SetActive(true);
        }

    }


    public void Pause()
    {
        GameIsInPause = true;
        foreach (GameObject e in Enemies)
        {
            e.GetComponent<Enemies_Controller>().CanShoot = false;
        }
    }

    public void QuitPause()
    {
        GameIsInPause = false;
        foreach (GameObject e in Enemies)
        {
            e.GetComponent<Enemies_Controller>().CanShoot = true;
        }
    }

}
