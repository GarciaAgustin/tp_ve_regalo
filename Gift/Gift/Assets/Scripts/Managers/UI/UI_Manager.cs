using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class UI_Manager : MonoBehaviour
{

    public static UI_Manager Instance;

    [SerializeField]
    GameObject Menu;

    [SerializeField]
    GameObject Options;

    [SerializeField]
    Slider SoundSlider;

    [SerializeField]
    Slider SFXSlider;

    [SerializeField]
    Slider SensitivitySlider;

    [SerializeField]
    GameObject Controls;

    [SerializeField]
    GameObject SelectTemplatesPanel;

    public bool TheUIisOpen;

    [SerializeField]
    List<Button> FirstPressedButtons = new List<Button>();

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        SoundSlider.value = PlayerPrefs.GetFloat("Volume");

        SoundSlider.onValueChanged.AddListener(delegate { SliderChanged(); });

        SFXSlider.value = PlayerPrefs.GetFloat("SFXs");

        SFXSlider.onValueChanged.AddListener(delegate { SliderChanged(); });

        if (Camera.main.GetComponent<CameraControl>() != null)
        {
            SensitivitySlider.value = PlayerPrefs.GetFloat("sensitivity");

            SensitivitySlider.onValueChanged.AddListener(delegate { SliderChanged(); });
        }
    }

    private void Start()
    {

    }

    public void SliderChanged()
    {
        Audio_Manager.Instance.ChangeVolumeMusic(SoundSlider.value);

        Audio_Manager.Instance.ChangeVolumeSFXs(SFXSlider.value);

        if(Camera.main.GetComponent<CameraControl>() != null)
        {
            Camera.main.GetComponent<CameraControl>().ChangeSensitivity(SensitivitySlider.value);
        }
    }



    private void Update()
    {
        if (Keyboard.current.tabKey.wasPressedThisFrame || Gamepad.current != null && Gamepad.current.startButton.wasPressedThisFrame)
        {
            OpenOptions();
        }
    }



    public void GotoItchio()
    {
        Application.OpenURL("https://live-games.itch.io/");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void OpenOptions()
    {
        EventSystem.current.SetSelectedGameObject(FirstPressedButtons[1].gameObject);
        TheUIisOpen = true;
        Options.gameObject.SetActive(true);
    }

    public void CloseOptions()
    {
        if(FirstPressedButtons[0] != null)
        {
            EventSystem.current.SetSelectedGameObject(FirstPressedButtons[0].gameObject);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(FirstPressedButtons[1].gameObject);
        }

        TheUIisOpen = false;
        Options.gameObject.SetActive(false);

        if (SelectTemplatesPanel != null && SelectTemplatesPanel.activeInHierarchy == true)
        {
            SelectTemplatesPanel.SetActive(false);
        }
    }

    public void GoToCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenControls()
    {
        EventSystem.current.SetSelectedGameObject(FirstPressedButtons[2].gameObject);
        Controls.SetActive(true);
    }

    public void CloseControls()
    {
        EventSystem.current.SetSelectedGameObject(FirstPressedButtons[1].gameObject);
        Controls.SetActive(false);
    }

    public void OpenSelectTemplates()
    {
        EventSystem.current.SetSelectedGameObject(FirstPressedButtons[3].gameObject);
        SelectTemplatesPanel.SetActive(true);
    }


    public void GotoPlataformerScene()
    {
        SceneManager.LoadScene("Plataformer_Scene");
    }

    public void GotoFirstPersonScene()
    {
        SceneManager.LoadScene("FirstPerson_Scene");
    }
}
