using System.Collections;
using TMPro;
using UnityEngine;
using System.Collections.Generic;
public class Dialogue_Manager : MonoBehaviour
{
    public TextMeshProUGUI dialogueText;
    public string[] dialogues;
    private int currentIndex = 0;
    public float letterDelay = 0.05f; // Tiempo de retraso entre cada letra

    private Coroutine currentCoroutine;

    private bool canAdvanceDialogue = true; // Variable para controlar si se puede avanzar al siguiente di�logo

    void Start()
    {
        DisplayNextDialogue();
    }

    void Update()
    {
        if (canAdvanceDialogue && Input.GetKeyDown(KeyCode.Space)) // Verificar si se puede avanzar y se presion� la tecla Espacio
        {
            DisplayNextDialogue();
        }
    }

    public void DisplayNextDialogue()
    {
        if (currentIndex < dialogues.Length)
        {
            currentCoroutine = StartCoroutine(WriteDialogue(dialogues[currentIndex]));
            currentIndex++;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator WriteDialogue(string dialogue)
    {
        canAdvanceDialogue = false; 

        dialogueText.text = ""; 
        foreach (char letter in dialogue)
        {
            dialogueText.text += letter; 
            yield return new WaitForSeconds(letterDelay); 
        }

        canAdvanceDialogue = true; 
        currentCoroutine = null; 
    }
}
